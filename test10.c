#include <stdio.h>
int main()
{
    int a,b,*p1,*p2;
    p1=&a;
    p2=&b;
    printf("Enter 2 integers:\n");
    scanf("%d%d",p1,p2);
    int sum,prod,dif,qou,rem;
    sum=*p1+*p2;
    prod=*p1**p2;
    dif=*p1-*p2;
    qou=*p1/ *p2;
    rem=*p1%*p2;
    printf("Sum=%d\nDifference=%d\nProduct=%d\nQuotient=%d\nRemainder=%d\n",sum,dif,prod,qou,rem);
    return 0;
}