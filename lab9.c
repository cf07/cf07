#include <stdio.h>
int swap(int *x, int *y)
{
   int temp;
   temp = *x;    
   *x = *y;      
   *y = temp;    
   return 0;
}
int main()
{
    int a,b,*p1,*p2;
    p1=&a;
    p2=&b;
    printf("Enter 2 integers:\n");
    scanf("%d%d",p1,p2);
    printf("Initial a=%d\nInitial b=%d\n",*p1,*p2);
    swap(&a,&b);
    printf("Final a=%d\nFinal b=%d\n",*p1,*p2);
    return 0;
}
