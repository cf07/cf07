#include <stdio.h>
int main()
{
    int i,a[10]= {1,2,3,5,11,13,17,19,23,0};
    printf ("The current arrangement is:\n");
    for (i=0;i<9;i++)
    {
        printf("%d ",a[i]);
    }
    for (i=10;i>4;i--)
    {
        a[i]=a[i-1];
    }
    a[4]=7;
    printf("\nThe new arrangement is:\n");
    for (i=0;i<10;i++)
    {
        printf("%d ",a[i]);
    }
    return 0;
}