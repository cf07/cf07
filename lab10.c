#include <stdio.h>
int main()
{
    FILE *input;
    char data;
    input=fopen("INPUT.txt","w");
    printf("Enter the data:\n");
    while((data=getchar())!=EOF)
    {
        putc(data,input);
    }
    fclose(input);
    printf("\n");
    input=fopen("INPUT.txt","r");
    printf("Contents of the file:\n");
    while((data=getc(input))!=EOF)
    {
        printf("%c",data);
    }
    fclose(input);
    return 0;
}