#include<stdio.h>
int main()
{
	int n,i,j=1;
	printf ("Enter a number from 1 to 100\n");
	scanf ("%d",&n);
	if (n>100 || n<1)
		printf ("Invalid input");
	//Now we loop we use i to equal to the number n so that it keeps getting multiplied. Then i is printed
	for (i=n;i<=100;i*=j)
	{
		printf("%d\n",i);
		j++;
		//To reset backto the main number
		i=n;
	}
	return 0;
}