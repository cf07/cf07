#include <stdio.h>
#include <string.h>
int main()
{
    int i;
    char word[20];
    printf("Enter a word or name less than 20 characters long\n");
    gets(word);
    while(word[i]!='\0')
    {
        if (word[i]>64 && word[i]<91)
            word[i]+=32;
        i++;
    }
    puts(word);
    return 0;
}
