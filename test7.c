#include <stdio.h>
int main()
{
    int a[10][10],i,j,m,n;
    printf("Enter the number of rows and coloumns in the original matrix. Must be less than 10x10:\n");
    scanf("%d%d",&m,&n);
    for(i=0;i<m;i++)
    {
        printf("Enter elements of row no %d\n",i+1);
        for (j=0;j<n;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("The original matrix is:\n");
    for(i=0;i<m;i++)
    {
        for (j=0;j<n;j++)
        {
            printf("%d ",a[i][j]);
        }
        printf("\n");
    }
    printf("The tranverse matrix is:\n");
    for(j=0;j<n;j++)
    {
        for (i=0;i<m;i++)
        {
            printf("%d ",a[j][i]);
        }
        printf("\n");
    }
    return 0;
}