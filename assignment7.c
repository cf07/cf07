#include <stdio.h>
int main()
{
    int a[4][3],i=0,j=0;
    int sumS[]={0,0,0,0};
    int sumP[]={0,0,0};
    for(i=0;i<4;i++)
    {
        for (j=0;j<3;j++)
        {
            switch (j)
            {
                case 0:
                    printf("Enter the amount made from the sale of Pepsi from salesman no %d\n",i+1);
                    scanf("%d",&a[i][j]);
                    sumS[i]+=a[i][j];
                    sumP[j]+=a[i][j];
                    break;
                case 1:
                    printf("Enter the amount made from the sale of Coca Cola from salesman no %d\n",i+1);
                    scanf("%d",&a[i][j]);
                    sumS[i]+=a[i][j];
                    sumP[j]+=a[i][j];
                    break;
                case 2:
                    printf("Enter the amount made from the sale of Sprite from salesman no %d\n",i+1);
                    scanf("%d",&a[i][j]);
                    sumS[i]+=a[i][j];
                    sumP[j]+=a[i][j];
                    break;
                default:
                    break;
            }
        }
    }
    printf("Salesman\tPepsi\tCocaCola Sprite\n");
    for(i=0;i<4;i++)
    {
        printf("    %d   \t",i+1);
        for (j=0;j<3;j++)
        {
            printf("  %d  \t",a[i][j]);
        }
        printf("\tThe total sum made by Salesman is %d\n",sumS[i]);
    }
    printf("Total sales\t");
    for (j=0;j<3;j++)
        {
            printf("  %d\t",sumP[j]);
        }
    return 0;
}