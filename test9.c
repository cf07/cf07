#include <stdio.h>
struct Stud
{
    int usn;
    char name[20],dept[10],sec;
    float marks;
};
int main()
{
    struct Stud S1,S2;
    printf("Enter student 1's details in order:\n1.USN\n2.Name\n3.Department\n4.Marks\n5.Section\n");
    scanf("%d ",&S1.usn);
    gets(S1.name);
    gets(S1.dept);
    scanf(" %f %c ",&S1.marks,&S1.sec);
    printf("Enter student 2's details in order:\n1.USN\n2.Name\n3.Department\n4.Marks\n5.Section\n");
    scanf(" %d ",&S2.usn);
    gets(S2.name);
    gets(S2.dept);
    scanf(" %f %c ",&S2.marks,&S2.sec);
    if (S1.marks>S2.marks)
        printf("Student 1's details in order as he has scored the higher marks:\n1.USN:%d\n2.Name:%s\n3.Department:%s\n4.Section:%c\n5.Marks:%f",S1.usn,S1.name,S1.dept,S1.sec,S1.marks);
    else if (S2.marks>S1.marks)
        printf("Student 2's details in order as he has scored the higher marks:\n1.USN:%d\n2.Name:%s\n3.Department:%s\n4.Section:%c\n5.Marks:%f",S2.usn,S2.name,S2.dept,S2.sec,S2.marks);
    else
        printf("Both the students have got the same marks");
    return 0;
}
